import Interface.showWidth;

public class Rectangle extends AbstractShapes implements showWidth, Interface.height {

    double width;
    double height;
    Rectangle (double width , double height){
        this.width = width;
        this.height = height;
    }


    @Override
    public void calculateArea() {
        area = width* height;
    }

    @Override
    public double width() {

        return width;
    }

    @Override
    public double height() {
        return height;
    }
}
