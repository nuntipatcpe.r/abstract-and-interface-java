import Interface.showWidth;

public class Triangle extends AbstractShapes implements showWidth {
    double radian;
    Triangle (double radian){
        this.radian = radian;
    }


    @Override
    public void calculateArea() {
        area =Math.PI*(radian*radian);
    }

    @Override
    public double width() {
        return 0;
    }
}
